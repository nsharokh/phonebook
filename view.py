from mixins import (SearchMixin,
                    UserDetailMixin)

class MenuViewView:
    content = '''
    1. Показать все контакты
    2. Удалить контакт
    3. Добавить/Редактировать контакт
    4. Показать контакт
    5. Выход
    '''
    def show(self, user=None):
        print(self.content)
        return input("Введите пункт меню: ")

class ShowUserView(SearchMixin):
    content = '''
    1. Показать все контакты
    2. Удалить контакт
    3. Добавить/Редактировать контакт
    4. Показать контакт
    5. Выход
    '''

class DeleteUserView:
    content = '''
    Юзер
    ----------------------
    Имя {}
    Отчечтво {}
    '''
    def show(self, user):
        print(self.content.format(user['name'], user['middle_name']))

class AllViewView:
    content = '''
    Все
    -----------------------
{}
    '''
    def show(self, users=None):
        res = ""
        for user in users:
            res += f"{user['name']} {user['surname']}\n"
        print(self.content.format(res))

class SearchViewView(SearchMixin):
    content = '''
    Введите id пользователя для отображения: 
    '''
    
class SnowViewView(UserDetailMixin):
    content = '''
    Юзер
    ----------------------
    id {}
    Имя {}
    Отчечтво {}
    '''
    error = ''
    
class SearchDeleteUserView(SearchMixin):
    content = '''
    Введите id пользователя для удаления: 
    '''

class ShowDeleteUserView(UserDetailMixin):
    content = '''
    Юзер c id {}
    Имя {}
    Отчеcтво {}
    удален успешно
    '''
    error = 'Ошибка! Пользователь не удалён'

class GetNewUserView(SearchMixin):
    content = '''
    Введите данные нового пользователя: 
    '''
class ShowSaveUserView(UserDetailMixin):
    content = '''
    Юзер c id {}
    Имя {}
    Отчеcтво {}
    добавлен успешно
    '''
    error = 'Ошибка! Пользователь не добавлен'
    def show(self, *args, **kwargs):
        super().show(*args, **kwargs)
        print('vasia') 
