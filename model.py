USERS = [{"user_id": 1,"name": "Вася", "surname":"Иванов"},
        {"user_id": 2,"name": "Коля", "surname":"Петров"},
        {"user_id": 3,"name": "Оля", "surname":"Иванова"}]

class Users:
    def get(self, user_id):
        for user in USERS:
            if user_id == user["user_id"]:
                return user
        return None

    def all(self):
        return USERS

    def delete(self, user_id):
        index = 'None'
        for user in USERS:
            if user_id == user["user_id"]:
                index = USERS.index(user)
                break
        if index == 'None':
            return False
        else:
            return USERS.pop(index)
    
    def save(self, user):
        USERS.append(user)
        return user

       