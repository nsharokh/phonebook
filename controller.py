from model import (
    Users) 
from view import (
    MenuViewView,
    AllViewView,
    SearchViewView,
    SnowViewView,
    SearchDeleteUserView,
    ShowDeleteUserView,
    GetNewUserView,
    ShowSaveUserView,
) 
from mixins import (SearchControllerMixin)

class MainViewController:    
    def show_view(self):
        obj = MenuViewView()
        return obj.show()

class SnowUserController:    
    def show_view(self, user_id):
        user = Users().get(user_id)
        SnowViewView().show(user)

class DeleteUserController:
    def show_view(self, user_id):
        user = Users().get(user_id)
        obj = MenuViewView()
        obj.show(user)

class AddEditUserController:    
    def show_view(self):
        obj = MenuViewView()
        obj.show()

class ListUsersController:
    def show_view(self):
        spisok = Users().all()
        AllViewView().show(spisok)

class SearchUserController(SearchControllerMixin):
    view_class = SearchViewView

class SearchDeleteUserController(SearchControllerMixin):
    view_class = SearchDeleteUserView

class SnowDeleteUserController:    
    def show_view(self, user_id):
        result = Users().delete(user_id)
        ShowDeleteUserView().show(result)
        
class GetNewUserController:
    def show_view(self):
        GetNewUserView().show()
        user_id = int(input('Введите ID пользователя '))
        name = input('Введите имя пользователя ')
        surname = input('Введите фамилию пользователя ')
        return {"user_id": user_id,"name": name, "surname":surname}

class SaveNewUserController:
    def show_view(self, newuser):
        result = Users().save(newuser)
        ShowSaveUserView().show(result)
 