class SearchMixin:
    def show(self):
        print(self.content)

class UserDetailMixin:
    def show(self, result):
        if result:
            print(self.content.format( result['user_id'], result['name'], result['surname']))
        else:
            print(self.error)

class SearchControllerMixin:
    def show_view(self):
        self.view_class().show()
        return input()
